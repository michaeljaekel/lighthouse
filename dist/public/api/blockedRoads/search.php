<?php

/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */

try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  if (isset($_REQUEST['queryString'])) {
    $queryString = (strip_tags($_REQUEST['queryString']));
  }
  if (isset($_REQUEST['searchAllRows'])) {
    $searchAllRows = (strip_tags($_REQUEST['searchAllRows']));
  }

  if ($searchAllRows == "false") {
    $filter = "and BlockedActive = 1";
  } else {
    $filter = "";
  }



  $query = "SELECT * FROM blockedroads 
    WHERE MATCH (BlockedText, BlockedTitle) AGAINST (:queryString IN BOOLEAN MODE) $filter";

  $stmt = $pdo->prepare($query);
  $stmt->bindParam(':queryString', $queryString);
  //$stmt->bindParam(':searchAllRows', $searchAllRows);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();

    /* Comment if this is not a SELECT query */
    $result['records'] = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /* Uncomment if this is an insert query */
    //$result['lastInsertedId'] = $pdo->lastInsertId();
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['error'] = $error;

  /* Finally send Data */
} finally {
  $result['params'] = $_POST;
  $result['filter'] = $searchAllRows;
  echo json_encode($result);
  $pdo = null;
}
