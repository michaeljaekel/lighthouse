<?php



/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */

try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  if (isset($_REQUEST['id'])) {
    $id = (strip_tags($_REQUEST['id']));
  }

  $query = "DELETE FROM blockedroads WHERE id=:id";

  $stmt = $pdo->prepare($query);
  $stmt->bindParam(':id', $id);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();

    /* Comment if this is not a SELECT query */
    //$result['records'] = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /* Uncomment if this is an insert query */
    //$result['lastInsertedId'] = $pdo->lastInsertId();
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['error'] = $error;

  /* Finally send Data */
} finally {
  echo json_encode($result);
  $pdo = null;
}
