<?php

/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */

ini_set('display_errors', 'On');
error_reporting(E_ALL);
try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  if (isset($_REQUEST['id'])) {
    $id = (strip_tags($_REQUEST['id']));
  }

  if (isset($_REQUEST['column'])) {
    $column = (strip_tags($_REQUEST['column']));
  }

  if (isset($_REQUEST['value'])) {
    $value = (strip_tags($_REQUEST['value']));
  }



  $query = "UPDATE blockedroads SET " . $column . " = :value WHERE id = :id";
  $stmt = $pdo->prepare($query);
  $stmt->bindParam(':id', $id);
  $stmt->bindParam(':value', $value);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();
    $result['data'] = $_POST;
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['error'] = $error;

  /* Finally send Data */
} finally {
  echo json_encode($result);
  $pdo = null;
}
