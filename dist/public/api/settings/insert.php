<?php


/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */

try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  $query = $pdo->insertIgnoreQueryFor('settings', 'id', $_POST);
  $stmt = $pdo->prepare($query);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();

    /* Uncomment if this is an insert query */
    $result['lastInsertedId'] = $pdo->lastInsertId();
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['error'] = $error;

  /* Finally send Data */
} finally {
  echo json_encode($result);
  $pdo = null;
}
