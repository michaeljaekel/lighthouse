# lighthouse

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Color Scheme
See [Color Scheme at Pantone ](https://www.pantone.com/articles/color-of-the-year/color-of-the-year-2020)