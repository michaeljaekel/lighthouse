<?php

/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */

try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  if (isset($_REQUEST['id'])) {
    $id = (strip_tags($_REQUEST['id']));
  }
  if (isset($_REQUEST['BlockedTitle'])) {
    $BlockedTitle = (strip_tags($_REQUEST['BlockedTitle']));
  }
  if (isset($_REQUEST['BlockedText'])) {
    $BlockedText = (strip_tags($_REQUEST['BlockedText']));
  }
  if (isset($_REQUEST['BlockedActive'])) {
    $BlockedActive = (strip_tags($_REQUEST['BlockedActive']));
  }
  if (isset($_REQUEST['BlockedFromDate'])) {
    $BlockedFromDate = (strip_tags($_REQUEST['BlockedFromDate']));
  }
  if (isset($_REQUEST['BlockedToDate'])) {
    $BlockedToDate = (strip_tags($_REQUEST['BlockedToDate']));
  }
  if (isset($_REQUEST['BlockedLink'])) {
    $BlockedLink = (strip_tags($_REQUEST['BlockedLink']));
  }
  if (isset($_REQUEST['BlockedOnMonitor'])) {
    $BlockedOnMonitor = (strip_tags($_REQUEST['BlockedOnMonitor']));
  }
  if (isset($_REQUEST['BlockedLinkText'])) {
    $BlockedLinkText = (strip_tags($_REQUEST['BlockedLinkText']));
  }
  if (isset($_REQUEST['BlockedType'])) {
    $BlockedType = (strip_tags($_REQUEST['BlockedType']));
  }
  $query = $pdo->updateParamQueryFor('blockedroads', 'id', $_POST);

  $stmt = $pdo->prepare($query);
  $stmt->bindParam(':id', $id);
  $stmt->bindParam(':BlockedTitle', $BlockedTitle);
  $stmt->bindParam(':BlockedText', $BlockedText);
  $stmt->bindParam(':BlockedActive', $BlockedActive);
  $stmt->bindParam(':BlockedFromDate', $BlockedFromDate);
  $stmt->bindParam(':BlockedToDate', $BlockedToDate);
  $stmt->bindParam(':BlockedLink', $BlockedLink);
  $stmt->bindParam(':BlockedOnMonitor', $BlockedOnMonitor);
  $stmt->bindParam(':BlockedLinkText', $BlockedLinkText);
  $stmt->bindParam(':BlockedType', $BlockedType);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();
    /* Comment if this is not a SELECT query */
    //$result['records'] = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /* Uncomment if this is an insert query */
    //$result['lastInsertedId'] = $pdo->lastInsertId();
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['query'] = $query;
  $result['data'] = $_POST;
  $result['error'] = $error;
  /* Finally send Data */
} finally {

  echo json_encode($result);
  $pdo = null;
}
