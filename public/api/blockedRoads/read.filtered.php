<?php


/**
/* API-Beschreibung für diese Datei
/* @returns {json, array} Records mit dem Abfrageergebnis (status: true) oder Hinweis auf Fehler in der Abfrage (status:false)
/*
/* ✏️ Michael Jäkel, michaeljaekel@mac.com
 */


ini_set('display_errors', 'On');
error_reporting(E_ALL);

try {
  require_once('../../../appconf/connectDB.php');
  $pdo = new myDBO($configDB);

  if (isset($_POST['filter'])) {
    $filter = (strip_tags($_POST['filter']));
  } else {
    $filter = "1";
  }


  if (isset($_POST['order'])) {
    $order = (strip_tags($_POST['order']));
  } else {
    $order = "id";
  }



  $query = "SELECT * FROM blockedroads WHERE $filter ORDER BY $order";
  $stmt = $pdo->prepare($query);

  if ($stmt->execute()) {

    /* Set status to true and get affected rows */
    $result['status'] = true;
    $result['affected'] = $stmt->rowCount();

    /* Comment if this is not a SELECT query */
    $result['records'] = $stmt->fetchAll(PDO::FETCH_ASSOC);

    /* Uncomment if this is an insert query */
    //$result['lastInsertedId'] = $pdo->lastInsertId();
  };

  /* Catch-Block */
} catch (Exception $error) {
  $result['status'] = false;
  $result['error'] = $error;

  /* Finally send Data */
} finally {
  $result['order'] = $order;
  $result['filter'] = $filter;
  $result['params'] = $_POST;
  $result['query'] = $query;
  echo json_encode($result);
  $pdo = null;
}
