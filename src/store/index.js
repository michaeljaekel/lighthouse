import {
  createStore
} from "vuex"
import axios from 'axios'
import tools from '@/tools/tools.js'





const store = createStore({
  state: {
    settings: [],
  },


  getters: {
    settings(state) {
      return state.settings
    },


    /** Die Settings für eine bestimmtes Anwendungsmodul auslesen
     * @param {string} state
     * @param {string} - Name des App-Moduls
     */
    appSettings: (state) => (appName) => {
      return state.settings.filter(setting => setting.app == appName)
    },


    /**
     * @param {string} appName - Name des AppModuls
     * @param {*} state 
     * @returns Hash mit Settings für die App.
     */
    appSettingsAsHash: (state) => (appName) => {
      const result = []
      const settings = state.settings.filter(setting => setting.app == appName)
      settings.forEach(element => result[element.property] = element.value);
      return result
    }
  },



  mutations: {

    /**
     * Eine Einstellung zu den Settings hinzufügen.
     *! ***** TEST *****
     * @param {*} state 
     */
    addSetting(state) {
      const obj = {
        id: 6,
        app: 'blockedRoads',
        property: 'showLive',
        value: 1,
        hint: 'Ein Value',
        timestamp: "2021-04-06 07:47:31"
      }
      state.settings.push(obj)
    },


    setSettings(state, newSettings) {
      state.settings = newSettings
    }
  },


  actions: {

    /**
     * Die Settings aus der Tabelle settings lesen und im Store über die Mutation setSettings im State ablegen.
     * Die Funktion muss async sein!!
     * @param {string} context - App-Modul für das die Einstellungen ausgelesen werden sollen. 
     */
    async readSettings(context) {
      const url = `public/api/settings/read.php`
      await axios
        .get(url)
        .then(result => context.commit('setSettings', result.data.records))
        .catch(error => console.error("Error: ", error, url))
    },




    /**
     * updateSettings()
     * @param {string} context - App-Modul für das die Einsellung geändert werden soll 
     * @param {*} newData - neuer Wert für die Einstellung
     */
    async updateSettings(context, newData) {
      const url = 'public/api/settings/update.php'
      newData.forEach(async element => {
        const data = tools.toFormData(element)
        await axios
          .post(url, data)
          .then(context.commit('setSettings', newData))
          .catch(error => console.error("Error: ", error, url))
      });
      context.commit('setSettings', newData)
    }


  }




})

export default store