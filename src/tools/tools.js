export default {


  getAPIDir() {
    return 'public/api/'
  },

  toFormData: function (obj) {
    var data = new FormData()
    for (var key in obj) {
      data.append(key, obj[key])
    }
    return data
  },

  isEmptyObject(aObject) {
    return Object.keys(aObject).length !== 0
  },

  isUndefined(myVar) {
    return typeof myVar == 'undefined' || myVar == null
  },

  isValue(myVar) {
    return !this.isUndefined(myVar) && this.isEmptyObject(myVar)
  },

  grep(items, callback) {
    var filtered = [],
      len = items.length,
      i = 0
    for (i; i < len; i++) {
      var item = items[i]
      var cond = callback(item)
      if (cond) {
        filtered.push(item)
      }
    }
    return filtered
  }
}