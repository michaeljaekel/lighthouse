import {
  createApp
} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'

import ElementPlus from 'element-plus';
// die element-plus-library wird in nova.scss geladen
// import 'element-plus/lib/theme-chalk/index.css';
import './styles/nova.scss'
import axios from 'axios'
import VueAxios from 'vue-axios'

import mitt from 'mitt'
const eventBus = mitt()


const app = createApp(App)
app.config.globalProperties.eventBus = eventBus

app
  .use(store)
  .use(router)
  .use(ElementPlus)
  .use(VueAxios, axios)
  .mount('#app')