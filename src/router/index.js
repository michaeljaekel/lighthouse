import {
  createRouter,
  createWebHashHistory
} from 'vue-router'
import Home from '../views/Home.vue'


const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/blockedroads/:view',
    name: 'BlockedRoads',
    component: () => import('@/views/BlockedRoads.vue'),
  }, {
    name: 'blockedroadsEditor',
    path: '/blockedroads/editor',
    component: () => import('@/components/blockedroads/editor/Editor.vue')
  }, {
    name: 'settings',
    path: '/settings',
    component: () => import('@/views/Settings.vue')
  }, {
    name: 'ausruecken',
    path: '/ausruecken/:view',
    component: () => import('@/views/Ausruecken.vue')
  }


]


const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router